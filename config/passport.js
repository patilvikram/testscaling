//config/passport.js

// loading local strategy

var LocalStrategy = require('passport-local').Strategy;


var User = require('../app/models/user');
//expose function to our app

module.exports= function(passport){


	passport.serializeUser(function(user, done) {
	  done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
	  User.findById(id, function(err, user) {
	    done(err, user);
	  });
	});
   	
	passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
    	},
    	function(req, email, password, done){
		console.log( "Looking for user with email",email);
		User.findOne({'local.email':email}, function(err,user){
			if(err)	{
			 console.log( "Error occured while fetching user ");
			 return done(err);
			}
			console.log("DB request processed ");
			if( user){
	
				return done(null, false,req.flash('signupMessage','Email already taken'));
			}
			else{

				var newUser=  new User();
				newUser.local.email=email;
				newUser.local.password=newUser.generateHash(password);
				console.log("Saving new user now");	
				// save the user
				newUser.save(function(err) {
					if( err)
						throw err;
					return done(null,newUser);
				});
				
			}
		})

	}));


}
