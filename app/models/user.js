// defining required values

var mongoose=require('mongoose');
var bcrypt =require('bcrypt-nodejs');
var Schema= mongoose.Schema;


var userSchema= new Schema({
	firstName:String,
	lastName:String,
	role: String,
	local:{
		email: String,
		password:String
	}
	
});


// methods for user table
// generating a hash value




// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', userSchema);

