// server file

var express= require("express");
var passport=require("passport");

var flash= require("connect-flash");
var mongoose = require("mongoose");



var app=express();
var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

// reading database configuration
var configDB=require("./config/database.js");
// connecting to db

console.log("Connecting to", configDB.url);
if(process.env.OPENSHIFT_MONGODB_DB_URL){
  configDB.url= process.env.OPENSHIFT_MONGODB_DB_URL + "local-vedantu";
}
mongoose.connect(configDB.url);


app.configure( function(){
	// setup express application
	app.use( express.logger("dev"));
	app.use(express.cookieParser());
	app.use(express.bodyParser());
	app.set('view engine','ejs');



	// required for passport
	app.use(express.session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
	app.use(passport.initialize());
	app.use(passport.session()); // persistent login sessions
	app.use(flash()); // use connect-flash for flash messages stored in session:

})

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport
require('./config/passport')(passport); 
// launch ======================================================================
server.listen(server_port, server_ip_address, function () {
  console.log( "Listening on " + server_ip_address + ", server_port " + port )
});
console.log('The magic happens on port ' + port);





